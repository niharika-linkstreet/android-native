package com.testapp.ls47.testapp;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dpizarro.autolabel.library.AutoLabelUI;
import com.dpizarro.autolabel.library.AutoLabelUISettings;


/**
 * Created by ls53 on 19/8/17.
 */

public class FormMcqActivity extends Activity {
    AutoCompleteTextView acv;
    Spinner dropdown;
    TextView question;
    AutoLabelUI mylabel;
    AutoCompleteTextView addlabeledit;
    Button addlabelbutton;
    LinearLayout McqLayout;
    String[] tags = {"Chennai", "Trichy", "Madurai", "Delhi", "mumbai", "Goa"};
    String[] dropDownOption = {"Select", "Option1", "Option2", "Option3", "Option4", "Option5"};

    public int indp(int x) {
        final float scale = getResources().getDisplayMetrics().density;
        int paddingindp = (int) (x * scale + 0.5f);
        return paddingindp;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_mcq_sub_layout);


//        AutocompleteView
        String[] options = getResources().getStringArray(R.array.AutocompleteOptions);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, options);

//        Adapter for Tags
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tags);
        acv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        acv.setAdapter(adapter);

//       Adapter for Dropdown
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dropDownOption);
        dropdown = (Spinner) findViewById(R.id.dropdown);
        dropdown.setAdapter(adapter2);

//      Adapter for tags
        mylabel = (AutoLabelUI) findViewById(R.id.label_view);
        addlabeledit = (AutoCompleteTextView) findViewById(R.id.tagedit);
        addlabeledit.setAdapter(adapter1);

        /*
        Calling tag creation method to create tag
         */
        tagcreation();

    }


    public void tagcreation() {
                /*
                Adding tag when a particular option selected
                 */
        addlabeledit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String textToAdd = addlabeledit.getText().toString();
                if (!textToAdd.isEmpty()) {
                    boolean success = mylabel.addLabel(textToAdd);
                    addlabeledit.getText().clear();
                }
            }
        });
                /*
                Adding tag when enter is pressed
                 */
        addlabeledit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            String textToAdd = addlabeledit.getText().toString();
                            if (!textToAdd.isEmpty()) {
                                boolean success = mylabel.addLabel(textToAdd);
                                addlabeledit.getText().clear();
                            }
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
                /*
                    Taglabel UI
                 */
        AutoLabelUISettings autoLabelUISettings = new AutoLabelUISettings.Builder()
                .withMaxLabels(10)
                .withIconCross(R.drawable.cross2)
                .withBackgroundResource(R.drawable.tags)
                .withLabelsClickables(false)
                .withShowCross(true)
                .withTextSize(indp(10))
                .withLabelPadding(indp(10))
                .withTextColor(Color.WHITE)
                .build();
        mylabel.setSettings(autoLabelUISettings);

    }


}


