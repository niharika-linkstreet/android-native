package com.testapp.ls47.testapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v4.media.RatingCompat;
import android.text.InputType;
import android.text.Layout;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ViewFlipper;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import static android.R.color.black;
import static android.bluetooth.BluetoothClass.Device.Major.PHONE;
import static android.graphics.Typeface.BOLD;
import static android.support.design.R.id.center_vertical;
import static com.testapp.ls47.testapp.R.drawable.calendar;
import static com.testapp.ls47.testapp.R.id.Email;
import static com.testapp.ls47.testapp.R.id.info;
import static com.testapp.ls47.testapp.R.id.linearMain;
import static com.testapp.ls47.testapp.R.id.parent;
import static com.testapp.ls47.testapp.R.id.text;
import static java.lang.Boolean.TRUE;

/**
 * Created by ls53 on 16/8/17.
 */

public class FormActivity extends Activity {
    private Calendar calendar;
    private TextView Datepicktext;
    private int year, month, day;
    //    private TimePicker timePicker;
    private TextView DateTimePicker;
    private String format = "";
    private int mHour;
    private int mMinute;
    String date_time = "";
    TextView inlinedateedittext;
    TextView inlinedatetimeedittext;
    EditText inlineEmailEdit;
    EditText inlineNumberEdit;
    EditText inlinePhoneEdit;
    EditText inlineTextEdit;

    String[] type = {"inlinedatetime", "inlinedate", "inlineNumber",  "inlinePhone","inlineText","inlineEmail"};
    String question = " This is the ques";


    public int indp(int x) {
        final float scale = getResources().getDisplayMetrics().density;
        int paddingindp = (int) (x * scale + 0.5f);
        return paddingindp;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_layout);

        LinearLayout.LayoutParams inlineformlayout = new LinearLayout.LayoutParams(indp(180), ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams inlineedittextlayout2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, indp(50));
        LinearLayout.LayoutParams inlineedittextlayout = new LinearLayout.LayoutParams(indp(124), indp(50));


        final LinearLayout lm = (LinearLayout) findViewById(R.id.linearMain);


        for (int i = 0; i < type.length; i++) {
            String datatype = type[i];

            switch (datatype) {
                case "inlinedate":
                    LinearLayout inlinedate = new LinearLayout(this);
                    inlinedate.setOrientation(LinearLayout.HORIZONTAL);

                    TextView inlinedatetext = new TextView(this);
                    inlinedatetext.setText(question);
                    inlinedatetext.setGravity(16);
                    inlinedatetext.setPadding(indp(25), indp(5), 0, indp(10));
                    inlinedatetext.setTextColor(Color.BLACK);
                    inlinedatetext.setTextSize(indp(5));
                    inlinedatetext.setBackgroundColor(getResources().getColor(R.color.grey));
                    inlinedatetext.setLayoutParams(inlineformlayout);
                    inlinedateedittext = new TextView(this);
                    inlinedateedittext.setLayoutParams(inlineformlayout);
                    inlinedateedittext.setGravity(16);
                    inlinedateedittext.setText("Date pick");
                    inlinedateedittext.setPadding(indp(25), indp(5), indp(5), indp(10));
                    inlinedateedittext.setTextColor(Color.BLACK);
                    inlinedateedittext.setTextSize(indp(5));
                    inlinedateedittext.setClickable(true);
                    inlinedateedittext.setLayoutParams(inlineedittextlayout);
                    inlinedateedittext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            datepick(view);
                        }
                    });

                    ImageView calendarimage = new ImageView(this);
                    calendarimage.setClickable(true);
                    calendarimage.setPadding(0, indp(8), 0, 0);
                    calendarimage.setImageResource(R.drawable.calendar);
                    View border = new View(this);
                    inlinedate.addView(inlinedatetext);
                    inlinedate.addView(inlinedateedittext);
                    inlinedate.addView(calendarimage);
                    lm.addView(inlinedate);
                    break;

                case "inlinedatetime":
                    LinearLayout inlinedatetime = new LinearLayout(this);
                    inlinedatetime.setOrientation(LinearLayout.HORIZONTAL);
                    TextView inlinedatetimetext = new TextView(this);
                    inlinedatetimetext.setText(question);
                    inlinedatetimetext.setGravity(16);
                    inlinedatetimetext.setPadding(indp(25), indp(5), 0, indp(10));
                    inlinedatetimetext.setTextColor(Color.BLACK);
                    inlinedatetimetext.setTextSize(indp(5));
                    inlinedatetimetext.setBackgroundColor(getResources().getColor(R.color.grey));
                    inlinedatetimetext.setLayoutParams(inlineformlayout);

                    inlinedatetimeedittext = new TextView(this);
                    inlinedatetimeedittext.setLayoutParams(inlineformlayout);
                    inlinedatetimeedittext.setGravity(16);
                    inlinedatetimeedittext.setText("Date and time");
                    inlinedatetimeedittext.setPadding(indp(20), indp(5), indp(5), indp(10));
                    inlinedatetimeedittext.setTextColor(Color.BLACK);
                    inlinedatetimeedittext.setTextSize(indp(5));
                    inlinedatetimeedittext.setClickable(true);
                    inlinedatetimeedittext.setLayoutParams(inlineedittextlayout);
                    inlinedatetimeedittext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DateTimePick(view);
                        }
                    });

                    ImageView datetimeimage = new ImageView(this);
                    datetimeimage.setClickable(true);
                    datetimeimage.setPadding(0, indp(10), 0, 0);
                    datetimeimage.setImageResource(R.drawable.datetime);
                    inlinedatetime.addView(inlinedatetimetext);
                    inlinedatetime.addView(inlinedatetimeedittext);
                    inlinedatetime.addView(datetimeimage);
                    lm.addView(inlinedatetime);
                    break;


                case "inlineEmail":
                    LinearLayout inlineEmail = new LinearLayout(this);
                    inlineEmail.setOrientation(LinearLayout.HORIZONTAL);
                    TextView inlineEmailtext = new TextView(this);
                    inlineEmailtext.setText(question);
                    inlineEmailtext.setGravity(16);
                    inlineEmailtext.setPadding(indp(25), indp(5), 0, indp(10));
                    inlineEmailtext.setTextColor(Color.BLACK);
                    inlineEmailtext.setTextSize(indp(5));
                    inlineEmailtext.setBackgroundColor(getResources().getColor(R.color.grey));
                    inlineEmailtext.setLayoutParams(inlineformlayout);

                    inlineEmailEdit = new EditText(this);
                    inlineEmailEdit.setLayoutParams(inlineedittextlayout2);
                    inlineEmailEdit.setGravity(16);
                    inlineEmailEdit.setHint("Email");
                    inlineEmailEdit.setPadding(indp(20), indp(5), indp(5), indp(10));
                    inlineEmailEdit.setTextColor(Color.BLACK);
                    inlineEmailEdit.setBackgroundColor(Color.argb(0,249,248,248));
                    inlineEmailEdit.setTextSize(indp(5));
                    inlineEmailEdit.setClickable(true);
                    inlineEmailEdit.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    inlineEmail.addView(inlineEmailtext);
                    inlineEmail.addView(inlineEmailEdit);
                    lm.addView(inlineEmail);
                    break;


                case "inlineNumber":
                    LinearLayout inlineNumber = new LinearLayout(this);
                    inlineNumber.setOrientation(LinearLayout.HORIZONTAL);
                    TextView inlineNumberText = new TextView(this);
                    inlineNumberText.setText(question);
                    inlineNumberText.setGravity(16);
                    inlineNumberText.setPadding(indp(25), indp(5), 0, indp(10));
                    inlineNumberText.setTextColor(Color.BLACK);
                    inlineNumberText.setTextSize(indp(5));
                    inlineNumberText.setBackgroundColor(getResources().getColor(R.color.grey));
                    inlineNumberText.setLayoutParams(inlineformlayout);

                    inlineNumberEdit = new EditText(this);
                    inlineNumberEdit.setLayoutParams(inlineedittextlayout2);
                    inlineNumberEdit.setGravity(16);
                    inlineNumberEdit.setHint("Number");
                    inlineNumberEdit.setPadding(indp(20), indp(5), indp(5), indp(10));
                    inlineNumberEdit.setTextColor(Color.BLACK);
                    inlineNumberEdit.setBackgroundColor(Color.argb(0,249,248,248));
                    inlineNumberEdit.setTextSize(indp(5));
                    inlineNumberEdit.setClickable(true);
                    inlineNumberEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
                    inlineNumber.addView(inlineNumberText);
                    inlineNumber.addView(inlineNumberEdit);
                    lm.addView(inlineNumber);
                    break;


                case "inlineText":
                    LinearLayout inlineText = new LinearLayout(this);
                    inlineText.setOrientation(LinearLayout.HORIZONTAL);
                    TextView inlinetext = new TextView(this);
                    inlinetext.setText(question);
                    inlinetext.setGravity(16);
                    inlinetext.setPadding(indp(25), indp(5), 0, indp(10));
                    inlinetext.setTextColor(Color.BLACK);
                    inlinetext.setTextSize(indp(5));
                    inlinetext.setBackgroundColor(getResources().getColor(R.color.grey));
                    inlinetext.setLayoutParams(inlineformlayout);

                    inlineTextEdit = new EditText(this);
                    inlineTextEdit.setLayoutParams(inlineedittextlayout2);
                    inlineTextEdit.setGravity(16);
                    inlineTextEdit.setBackgroundColor(Color.argb(0,249,248,248));
                    inlineTextEdit.setHint("Text");
                    inlineTextEdit.setPadding(indp(20), indp(5), indp(5), indp(10));
                    inlineTextEdit.setTextColor(Color.BLACK);
                    inlineTextEdit.setTextSize(indp(5));
                    inlineTextEdit.setClickable(true);
                    inlineTextEdit.setInputType(InputType.TYPE_CLASS_TEXT);
                    inlineText.addView(inlinetext);
                    inlineText.addView(inlineTextEdit);

                    lm.addView(inlineText);
                    break;


                case "inlinePhone":
                    LinearLayout inlinePhone = new LinearLayout(this);
                    inlinePhone.setOrientation(LinearLayout.HORIZONTAL);
                    TextView inlinePhonetext = new TextView(this);
                    inlinePhonetext.setText(question);
                    inlinePhonetext.setGravity(16);
                    inlinePhonetext.setPadding(indp(25), indp(5), 0, indp(10));
                    inlinePhonetext.setTextColor(Color.BLACK);
                    inlinePhonetext.setTextSize(indp(5));
                    inlinePhonetext.setBackgroundColor(getResources().getColor(R.color.grey));
                    inlinePhonetext.setLayoutParams(inlineformlayout);

                    inlinePhoneEdit = new EditText(this);
                    inlinePhoneEdit.setLayoutParams(inlineedittextlayout2);
                    inlinePhoneEdit.setGravity(16);
                    inlinePhoneEdit.setHint("Phone");
                    inlinePhoneEdit.setBackgroundColor(Color.argb(0,249,248,248));
                    inlinePhoneEdit.setPadding(indp(20), indp(5), indp(5), indp(10));
                    inlinePhoneEdit.setTextColor(Color.BLUE);
                    inlinePhoneEdit.setTextSize(indp(5));
                    inlinePhoneEdit.setClickable(true);
                    inlinePhoneEdit.setInputType(InputType.TYPE_CLASS_PHONE);
                    inlinePhone.addView(inlinePhonetext);
                    inlinePhone.addView(inlinePhoneEdit);
                    lm.addView(inlinePhone);
                    break;

            }

              View border = new View(this);
              border.setBackgroundColor(Color.GRAY);
              LinearLayout.LayoutParams borderlayout = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,indp(3));
              border.setLayoutParams(borderlayout);
              lm.addView(border);
        }

        Button submit = new Button(this);
        submit.setText("Submit");
        LinearLayout.LayoutParams buttonlayout = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,indp(50));
        submit.setLayoutParams(buttonlayout);
        lm.addView(submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Info",makeJSON());
            }
        });



    }

    public String makeJSON() {
        JSONArray jArr = new JSONArray();
        JSONObject jObj = new JSONObject();
        try {

            jObj.put("date", inlinedateedittext.getText());
            jObj.put("date and time",inlinedatetimeedittext.getText());
            jObj.put("Number", inlineNumberEdit.getText());
            jObj.put("Phone",inlinePhoneEdit.getText());
            jObj.put("Text", inlineTextEdit.getText());
            jObj.put("Email",inlineEmailEdit.getText());



            jArr.put(jObj);

        } catch (Exception e) {
            System.out.println("Error:" + e);
        }

        String Json = jArr.toString();
        return Json;
    }


    public void datepick(View view) {

        Datepicktext = findViewById(R.id.datepicktext);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDialog(999);
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // arg1 = year
            // arg2 = month
            // arg3 = day
            inlinedateedittext.setText(new StringBuilder().append(day).append("-")
                    .append(month).append("-").append(year));
        }
    };




    public void DateTimePick(View view) {

        DateTimePicker = (TextView) findViewById(R.id.datetimetext);
        Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        datePicker();

    }


    private void datePicker(){
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {

                        date_time = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                        //*************Call Time Picker Here ********************
                        timePicker();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void timePicker(){
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        mHour = hourOfDay;
                        mMinute = minute;
                        if (hourOfDay == 0) {
                            hourOfDay += 12;
                            format = "AM";
                        } else if (hourOfDay == 12) {
                            format = "PM";
                        } else if (hourOfDay > 12) {
                            hourOfDay -= 12;
                            format = "PM";
                        } else {
                            format = "AM";
                        }
                        inlinedatetimeedittext.setText(date_time+" "+"-" +hourOfDay + ":" + minute +format);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }
}
















