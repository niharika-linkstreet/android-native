package com.testapp.ls47.testapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class LayoutActivity extends AppCompatActivity {

    public static final int RequestPermissionCode = 1;
    ListView listView;
    ArrayList<String> StoreContacts;
    ArrayAdapter<String> arrayAdapter;
    Cursor cursor;
    String name, phonenumber;
//    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scrolling);
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(Html.fromHtml("<h2>Contact List</h2>"));

        listView = (ListView) findViewById(R.id.myListView);

//        button = (Button) findViewById(R.id.buttonAddContacts);

        StoreContacts = new ArrayList<String>();

        EnableRuntimePermission();

//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                GetContactsIntoArrayList();
//
//                arrayAdapter = new ArrayAdapter<String>(
//                        LayoutActivity.this,
//                        android.R.layout.simple_list_item_1,
//                        R.id.textView, StoreContacts
//                );
//
//                listView.setAdapter(arrayAdapter);
//
//
//            }
//        });

    }

    public void GetContactsIntoArrayList() {

        cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        while (cursor.moveToNext()) {

            name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

            phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            StoreContacts.add(name + " " + ":" + " " + phonenumber);
        }

        cursor.close();

    }

    public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {

            Toast.makeText(this, "CONTACTS permission allows us to access CONTACTS", Toast.LENGTH_LONG).show();

        }
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, RequestPermissionCode);

    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {

                    GetContactsIntoArrayList();

                    arrayAdapter = new ArrayAdapter<String>(LayoutActivity.this, android.R.layout.simple_list_item_1, StoreContacts);

                    listView.setAdapter(arrayAdapter);
                    Toast.makeText(LayoutActivity.this, "Contacts retrieved", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(LayoutActivity.this, "Permission Canceled, Now your application cannot access CONTACTS.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }


}


//public class LayoutActivity extends Activity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_scrolling);
//        TextView textView = (TextView) findViewById(R.id.textView);
//        textView.setText(Html.fromHtml("<h2>Form List</h2>"));
//        ListView myListView = (ListView) findViewById(R.id.myListView);
//        final ArrayList<String> myForms = new ArrayList<>();
//        myForms.add("Sales");
//        myForms.add("Account");
//        myForms.add("HR");
//        myForms.add("Employees Information");
//        myForms.add("Personal Info");
//        myForms.add("Activity");
//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, myForms);
//        myListView.setAdapter(arrayAdapter);
//        myListView.setOnItemClickListener(
//                new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                        Log.i("Form tapped",myForms.get(position));
//                    }
//                }
//        );
//    }
//}
