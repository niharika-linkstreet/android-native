package com.testapp.ls47.testapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
    public EditText myInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RelativeLayout backgroundRelativeLayout = (RelativeLayout) findViewById(R.id.main_view);
        ImageView logoImageView = (ImageView) findViewById(R.id.imageViewLogo);
        myInput = (EditText) findViewById(R.id.main_input);
        myInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(myInput);
                }
            }
        });
        backgroundRelativeLayout.setOnClickListener(this);
        logoImageView.setOnClickListener(this);

    }

    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void onConvert(View view) {
        String dollarVal = myInput.getText().toString();
        if (dollarVal != null && !dollarVal.isEmpty()) {
            double dollars = Double.parseDouble(dollarVal.toString());
            double rupees = dollars * 60;
            final Snackbar snackBar = Snackbar.make(findViewById(R.id.main_view), String.format("Rs. %s", rupees), Snackbar.LENGTH_INDEFINITE);
            snackBar.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackBar.dismiss();
                }
            }).show();
            myInput.setText("");
            hideKeyboard(view);
        } else {
            Toast.makeText(this, "Enter value first",
                    Toast.LENGTH_LONG).show();
        }
//        ImageView myImage = (ImageView) findViewById(R.id.imageView);
//        myImage.setImageResource(R.drawable.offline);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.main_view || view.getId() == R.id.imageViewLogo) {
            Log.i(String.valueOf(view.getId()), "id>>>>>>>>>");
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromInputMethod(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void goToAudio(View view) {
        Intent audio = new Intent(MainActivity.this, AudioActivity.class);
        startActivity(audio);
    }

    public void goToLayout(View view) {
        Intent layout = new Intent(MainActivity.this, LayoutActivity.class);
        startActivity(layout);
    }

    public void goToMaps(View view) {
        Intent maps = new Intent(MainActivity.this, PlacesActivity.class);
        startActivity(maps);
    }

    public void goToImage(View view) {
        Intent image = new Intent(MainActivity.this, ImageActivity.class);
        startActivity(image);
    }
    public void goToFirebase(View view){
        Intent firebase = new Intent(MainActivity.this, FirebaseActivity.class);
        startActivity(firebase);
    }

    public void goToFormActivity(View view){
        Intent formActivity = new Intent(MainActivity.this, FormActivity.class);
        startActivity(formActivity);
    }

    public void goToInline(View view){
        Intent InlineActivity = new Intent(MainActivity.this, FormInlineActivity.class);
        startActivity(InlineActivity);
    }

    public void goToStacked(View view){
        Intent StackedActivity = new Intent(MainActivity.this, formStackedActivity.class);
        startActivity(StackedActivity);
    }

    public void goToPlaceholder(View view){
        Intent placeholderActivity = new Intent(MainActivity.this, FormPlaceHolderActivity.class);
        startActivity(placeholderActivity);
    }

    public void goToMcqActivity(View view){
        Intent McqActivity = new Intent(MainActivity.this,FormMcqActivity.class);
        startActivity(McqActivity);
    }
}
