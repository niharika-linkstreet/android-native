package com.testapp.ls47.testapp;
//
//import android.Manifest;
//import android.content.Context;
//import android.content.pm.PackageManager;
//import android.location.Criteria;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.content.ContextCompat;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.MarkerOptions;
//
//public class Maps2Activity extends FragmentActivity implements OnMapReadyCallback {
//
//    LocationManager locationManager;
//    LocationListener locationListener;
//    private GoogleMap mMap;
//    public Criteria criteria;
//    public String bestProvider;
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == 1) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Log.i(">>>>>>>>>>", String.valueOf(grantResults[0]));
//                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
//                    // current location
//                }
//            }
//        }
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_maps2);
//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
////        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
////        locationListener = new LocationListener() {
////            @Override
////            public void onLocationChanged(Location location) {
////                Log.i("Location", location.toString());
////            }
////
////            @Override
////            public void onStatusChanged(String s, int i, Bundle bundle) {
////
////            }
////
////            @Override
////            public void onProviderEnabled(String s) {
////
////            }
////
////            @Override
////            public void onProviderDisabled(String s) {
////
////            }
////        };
////        // if device running  SDK<23
////        if (Build.VERSION.SDK_INT < 23) {
////            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
////            // current location
////        } else {
////            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
////                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
////            } else {
////                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
////                // current location
////            }
////        }
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        locationManager.removeUpdates(locationListener);
//
//    }
//    /**
//     * Manipulates the map once available.
//     * This callback is triggered when the map is ready to be used.
//     * This is where we can add markers or lines, add listeners or move the camera. In this case,
//     * we just add a marker near Sydney, Australia.
//     * If Google Play services is not installed on the device, the user will be prompted to install
//     * it inside the SupportMapFragment. This method will only be triggered once the user has
//     * installed Google Play services and returned to the app.
//     */
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
//        criteria = new Criteria();
//        bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true)).toString();
//        locationListener = new LocationListener() {
//            @Override
//            public void onLocationChanged(Location location) {
//                Log.i(">>>>>>>>>>", String.valueOf(location));
//                locationManager.removeUpdates(locationListener);
//                LatLng banglore = new LatLng(location.getLatitude(), location.getLongitude());
//                mMap.clear();
//                mMap.addMarker(new MarkerOptions().position(banglore).title("You").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(banglore));
//                Toast.makeText(Maps2Activity.this, location.toString(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onStatusChanged(String s, int i, Bundle bundle) {
//
//            }
//
//            @Override
//            public void onProviderEnabled(String s) {
//
//            }
//
//            @Override
//            public void onProviderDisabled(String s) {
//
//            }
//        };
//        if (Build.VERSION.SDK_INT < 23) {
//            Log.i(">>>>>>>>>>23", String.valueOf(bestProvider));
//            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
//            // current location
//        } else {
//            Log.i(">>>>>>>>>>1", String.valueOf(bestProvider));
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//                Log.i(">>>>>>>>>>2", String.valueOf(bestProvider));
//            } else {
//                Log.i(">>>>>>>>>>=3", String.valueOf(bestProvider));
//                locationManager.requestLocationUpdates(bestProvider, 1000, 0, locationListener);
//                Location lastKnownLocation = locationManager.getLastKnownLocation(bestProvider);
//                if(lastKnownLocation!=null) {
//                    Log.i(">>>>>>>>>>4", String.valueOf(bestProvider));
//                    LatLng banglore = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
//                    mMap.clear();
//                    mMap.addMarker(new MarkerOptions().position(banglore).title("You").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
//                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(banglore, 10));
//                    Toast.makeText(Maps2Activity.this, lastKnownLocation.toString(), Toast.LENGTH_SHORT).show();
//                }else{
//                    Log.i(">>>>>>>>>>5", String.valueOf(bestProvider));
//                    locationManager.requestLocationUpdates(bestProvider, 1000, 0, locationListener);
////                    Location knownLocation = locationManager.getLastKnownLocation(bestProvider);
////                    Log.i(">>>>>>>>>6",knownLocation.toString());
//                }
//                // current location
//            }
//        }
////        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID); // for satellite view
//        // Add a marker in Sydney and move the camera
////        LatLng banglore = new LatLng(12.9716, 77.5946);
////
////        mMap.addMarker(new MarkerOptions().position(banglore).title("Bangalore").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
////        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(banglore, 10));
//    }
//}
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class Maps2Activity extends AppCompatActivity {

    LocationManager locationManager;

    LocationListener locationListener;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {

                Log.i("Location", location.toString());

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }

        };

        // If device is running SDK < 23

        if (Build.VERSION.SDK_INT < 23) {

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        } else {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                // ask for permission

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);


            } else {

                // we have permission!

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

            }

        }

    }
}
