

## Instructions [UBUNTU]
#### 1. Installing dependencies
1. Java

    > Refer https://java.com/en/download/

2. Android studio and Android SDK [Android studio has SDK manager which can be used to download the SDK]

    > Refer https://developer.android.com/studio/index.html


#### 2. Setup
1. Make sure your java's location is added to `$PATH` variable
    1. To test

        >  Run `java -version`

    2. To add folder to path variable

        > Run `export JAVA_HOME=$(path/to/javahome)` and `export PATH=${JAVA_HOME}/bin:$PATH` in your terminal

2. Add Android sdk tools directory and Android platform-tools directory to `$PATH` variable.

    > Run `export PATH=${PATH}:~/Android/Sdk/platform-tools:~/Android/Sdk/tools`. [Assuming your android sdk sits on ~/Android/Sdk]

3. Create `ANDROID_HOME` environment variable which points to android sdk location

    > Run `export ANDROID_HOME=~/Android/Sdk`.

4. To make export statements execute everytime on boot.

    > Run `vi ~/.bash_profile` and append all export statement to end of that file and press `[esc]` then `:wq` and then `[enter]` . Then finally run `source ~/.bash_profile` to execute it [this is only for the first time].
  Reference Link :- http://coolestguidesontheplanet.com/add-shell-path-osx/

#### 3. Connecting via ADB
1. Connect the device to the developer machine.
2. Enable `usb debugging` in your android device.
3. Authorize the connection in mobile device
4. Run `adb devices` to list the device

#### 4. Running project on device via Android Studio
1. Click the play icon on the toolbar

#### 5. Running project on device via command line

[Note: Make sure sdk, build tools and gradle is downloaded. Use android studio to set up all these before running this command]

1. Run `gradlew installDebug` to build and install the app to the device.

[REFER: https://developer.android.com/studio/build/building-cmdline.html for more commands]

#### Build Types

1. Debug

    > Run `gradlew installDebug` to install default app (pointing to stage server) in debug mode

2. Live

    > Run `gradlew installLive` to install app pointing to live server in debug mode

3. Staging

    > Run `gradlew installStage` to install app pointing to stage server in debug mode

4. Release

    Singing certificate wont be available as it has to be done manually. This apk points to live server


#### Extra Commands

1. Run `gradlew build` to build all four build types.
2. Run `gradlew tasks` to get the list of tasks.
3. use `assemble` instead of `install` to build the apk and not install it. For example, for live build, use `assembleLive` to generate the apk instead of `installLive` which installs the apk to connected device
4. Run `adb shell dumpsys window | grep cur= |tr -s " " | cut -d " " -f 4|cut -d "=" -f 2` to get device resolution
5. Run `adb shell getprop | grep density` to get device density 

